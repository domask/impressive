from .models import Category, Item
from django.contrib import admin

admin.site.register((Item, Category))

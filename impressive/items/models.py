from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    title = models.CharField(max_length=20)
    description = models.CharField(max_length=200)
    slug = models.SlugField(max_length=20)

    class Meta:
        verbose_name_plural = "categories"

    def __unicode__(self):
        return self.title


class Item(models.Model):
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, default="slug")
    url = models.CharField(max_length=200)
    category = models.ForeignKey(Category)
    image = models.CharField(max_length=200)
    host = models.CharField(max_length=20)
    user = models.ForeignKey(User, default="1")
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    up = models.IntegerField(default="0")
    down = models.IntegerField(default="0")

    def __unicode__(self):
        return self.title

from django.shortcuts import render, redirect
from .models import Item, Category
from .forms import ShareboxForm


form = ShareboxForm()


def index(request):
    items = Item.objects.all().order_by('date_created').reverse()
    data = {'items': items, 'sharebox_form': form}
    return render(request, "items/grid.html", data)


def category(request, category):
    try:
        c = Category.objects.get(slug=category.lower())
        items = Item.objects.filter(category=c.id).order_by('date_created').reverse()
        data = {'title': c.title, 'items': items, 'sharebox_form': form}
        #template = "items/index.html" if c.id != 3 else "items/grid.html"
        template = 'items/grid.html'
        return render(request, template, data)
    except Category.DoesNotExist:
        return render(request, "items/category_404.html")

def submit(request):
    form = ShareboxForm(data=request.POST)
    if request.method == 'POST':
        if form.is_valid():
            print 'valid'
            form.save()
            return redirect('/')
        else:
            print form.errors
            return redirect(request, form=form)
    return redirect('/')

from django.forms import ModelForm, TextInput, Select, HiddenInput
from .models import Item

class ShareboxForm(ModelForm):
    class Meta:
        model = Item
        include = ('title', 'url', 'category', 'image', 'host')  
        exclude = ('up', 'down', 'slug', 'user') # parasyt testa
        widgets = {
            'title': TextInput(attrs={'placeholder': 'Title'}),
            'url':  TextInput(attrs={'placeholder':'URL'}),
            'category': Select(attrs={'initial': 'Category'}),
            'image': HiddenInput(),
            'host': HiddenInput()
        }

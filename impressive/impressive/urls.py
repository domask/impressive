from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'items.views.index'),
    url(r'^link/', 'links.views.check'),
    url(r'^submit/', 'items.views.submit'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'(?P<category>[A-Za-z0-9]{1,30})/$', 'items.views.category'),
)


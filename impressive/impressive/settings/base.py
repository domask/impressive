from os.path import abspath, basename, dirname, join, normpath
from os import environ

from sys import path

def get_env_setting(setting):
    """ Get the environment setting or return exception """
    try:
        return environ[setting]
    except KeyError:
        print "WARNING! The %s environment variable is not set!" % setting

################ PATH CONFIG
# Absolute filesystem path to the Django project directory 
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name
SITE_NAME = basename(DJANGO_ROOT)

# Appending project to pythonpath to avoid typing project name in imports
path.append(DJANGO_ROOT)
############### END PATH CONFIG


############### DEBUG CONFIG
DEBUG = False

TEMPLATE_DEBUG = DEBUG
############### END DEBUG CONFIG


############### MANAGER CONFIG
ADMINS = (
    ('Example', 'example@example.com'),
)

MANAGERS = ADMINS
############### END MANAGER CONFIG


############### GENERAL CONFIG
TIME_ZONE = 'Europe/Copenhagen'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

USE_TZ = True
################ END GENERAL CONFIG


################ MEDIA CONFIG
MEDIA_ROOT = normpath(join(SITE_ROOT, 'media'))

MEDIA_URL = '/media/'
################ END MEDIA CONFIG


################ STATIC FILE CONFIG
STATIC_ROOT = normpath(join(SITE_ROOT, 'assets'))

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    normpath(join(SITE_ROOT, 'static')),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
################ END STATIC FILE CONFIG


################ SECRET KEY CONFIG
# Note: This key is only used for development and testing
SECRET_KET = r"qk$czomedk_7vdzc!yp+s+q2qiayv(t+g(4lof_q$+*b#3e&amp;h1"
################ END SECRET KEY CONFIG


################ TEMPLATE CONFIG
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_DIRS = (
    normpath(join(SITE_ROOT, 'templates')),
)
################ END TEMPLATE CONFIG


################ MIDDLEWARE CONFIG
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)
################ END MIDDLEWARE CONFIG


################ URL CONFIG
ROOT_URLCONF = '%s.urls' % SITE_NAME
################ END URL CONFIG


################ APP CONFIG
DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.humanize',
)

THIRD_PARTY_APPS = (
    'south',
)

LOCAL_APPS = (
    'items',
    'links',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS
################ END APP CONFIG


################ WSGI CONFIG
WSGI_APPLICATION = 'wsgi.application'
################ END WSGI CONFIG


################ LOGGING CONFIGURATION
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
################ END LOGGING CONFIGURATION

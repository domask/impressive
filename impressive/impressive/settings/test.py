from base import *

INSTALLED_APPS += (
    'django_coverage',
)

TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

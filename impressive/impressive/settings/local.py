from os.path import join, normpath
from base import *

############### DEBUG CONFIG
DEBUG = True

TEMPLATE_DEBUG = DEBUG
############### END DEBUG CONFIG


############### DATABASE CONFIG
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'impressive',
        'USER': get_env_setting('DB_USER'),
        'PASSWORD': get_env_setting('DB_PASS'),
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
############### END DATABASE CONFIG


############### APPS CONFIG
# Note: only for development or testing purposes
INSTALLED_APPS += (
    'debug_toolbar',
)
############### END APPS CONFIG


############### TOOLBAR CONFIG
INTERNAL_IPS = ('127.0.0.1',)

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.version.VersionDebugPanel',
    'debug_toolbar.panels.timer.TimerDebugPanel',
    'debug_toolbar.panels.settings_vars.SettingsVarsDebugPanel',
    'debug_toolbar.panels.headers.HeaderDebugPanel',
    'debug_toolbar.panels.request_vars.RequestVarsDebugPanel',
    'debug_toolbar.panels.template.TemplateDebugPanel',
    'debug_toolbar.panels.sql.SQLDebugPanel',
    'debug_toolbar.panels.signals.SignalDebugPanel',
    'debug_toolbar.panels.logger.LoggingPanel',
)

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TEMPLATE_CONTEXT': True,
}
############### END TOOLBAR CONFIG

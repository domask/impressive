$(document).ready(function() {
    $('#id_url').focusout(function() {
        $.post('/link/', {'url':this.value},
            function(data) {
                $('#id_url').addClass('light_green');
                if(data.title != undefined) {
                    $('#id_title').val(data.title).addClass('light_green');
                }
                var o = {'video': 1, 'article': 2}
                if(o[data.type] != undefined) {
                    $('#id_category').val(o[data.type]).addClass('light_green');
                }
                if(data.image != undefined) { 
                    $('#id_image').val(data.image);
                }
                if(data.host != undefined) {
                    $('#id_host').val(data.host);
                }
            }, "json").error(function() {
                $('#id_url').addClass('light_red');
            });
    }); 
});

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function getCookie(name) {
    var cookieValue = null;
    if(document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for(var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if(cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({
    crossDomain: false,
    beforeSend: function(xhr, settings) {
        if(!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

from django.test import TestCase
from django.test.client import Client


c = Client()


class ViewTest(TestCase):
    def test_get_method(self):
        response = c.get('/link/')
        self.assertEqual(response.status_code, 405)

    def test_post_without_parameter(self):
        response = c.post('/link/')
        self.assertEqual(response.status_code, 404)

    def test_post_with_empty_url(self):
        response = c.post('/link/', {'url': ''})
        self.assertEqual(response.status_code, 404)

    def test_post_with_invalid_url(self):
        response = c.post('/link/', {'url': 'a.b.c.d'})
        self.assertEqual(response.status_code, 404)

    def test_post_with_valid_url_with_og(self):
        response = c.post('/link/', {'url': 'ogp.me'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-type'], 'application/json')

    def test_post_with_valid_url_without_og_(self):
        response = c.post('/link/', {'url': 'google.com'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, '')

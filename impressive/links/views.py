import opengraph
import urllib2
import re
from urlparse import urlparse
from django.http import HttpResponse


def check(request):
    (content, status, mimetype) = ('', 404, None)
    if request.method == "POST":
        if request.POST.get("url", False) or False:
            try:
                url = request.POST.get("url")
                parsed_url = urlparse(url)
                parsed_url = parsed_url[1].split('.')
                if parsed_url[-1] == 'uk':
                    host = parsed_url[-3]+'.'+parsed_url[-2]+'.'+parsed_url[-1]
                else:
                    host = parsed_url[-2:2][0]+"."+parsed_url[-1:][0]
                if not re.match(r'^https?://', url):
                    url = "http://"+url
                g = opengraph.OpenGraph(url=url)
                if g.is_valid():
                    g['host'] = host
                    content = g.to_json()
                    mimetype = 'application/json'
                status = 200
            except urllib2.URLError:
                pass  # returns 404
    else:
        status = 405
    return HttpResponse(content, mimetype, status)
